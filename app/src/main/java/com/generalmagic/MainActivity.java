package com.generalmagic;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void calculateSum(View view)
    {
        EditText firstEditText = findViewById(R.id.firstEditText);
        EditText secondEditText = findViewById(R.id.secondEditText);
        int nr1 = Integer.parseInt(firstEditText.getText().toString());
        int nr2 = Integer.parseInt(secondEditText.getText().toString());
        String str = getResources().getString(R.string.sum_is);
        Toast.makeText(this, str + ": " + (nr1 + nr2), Toast.LENGTH_LONG).show();
    }
}
